package main

import (
	"strings"
)

func main() {
	stringReverse("hello")
}

func stringReverse(s string) string {
	var reversed []string
	split := strings.Split(s, "")
	for i := len(split) - 1; i >= 0; i-- {
		reversed = append(reversed, split[i])
	}
	reversedStr := strings.Join(reversed, "")
	return reversedStr
}
