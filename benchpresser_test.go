package main

import "testing"

func TestStringReverse(t *testing.T) {
	str := stringReverse("meow")
	if str != "woem" {
		t.Errorf("Expected reversed string to be woem, but got %v", str)
	}
}

const testStr = "test"

func benchmarkStringReverse(str string, stringReverse func(string) string, b *testing.B) {
	for n := 0; n < b.N; n++ {
		stringReverse(str)
	}
}
func BenchmarkStringReverse(b *testing.B) {
	benchmarkStringReverse(testStr, stringReverse, b)
}
